﻿
DECLARE @TotalSignaturesBE int = 0,
        @TotalSignaturesClaimed int = 0,
        @TotalBossSignatures int = 0
SELECT @TotalSignaturesBE = SUM(ISNULL(SignaturesCountedBE, 0)),
	   @TotalSignaturesClaimed = SUM(ISNULL(SignaturesClaimed, 0)),
	   @TotalBossSignatures = SUM(ISNULL(BossSignatures, 0))
FROM eddsdbo.Document WITH(NOLOCK) 
WHERE circulator = @CirculatorArtifactId

UPDATE eddsdbo.Circulator 
SET SignaturesCountedBE = @TotalSignaturesBE, 
    SignaturesClaimed = @TotalSignaturesClaimed, 
    BossSignatures = @TotalBossSignatures
WHERE ArtifactID = @CirculatorArtifactId


