﻿using kCura.EventHandler;
using kCura.EventHandler.CustomAttributes;
using kCura.Relativity.Client;
using System;
using System.Runtime.InteropServices;

namespace BE.PetitionReviewer.Resources
{

    [Guid("987A1866-4C47-4ED6-888C-793F2CA24E2E")]
    [Description("Petition Reviewer Page Interaction")]
    public class DocumentPageInteraction : PageInteractionEventHandler
    {
        public override Response PopulateScriptBlocks()
        {
            Response retVal = new Response();
            retVal.Success = true;
            retVal.Message = string.Empty;
            var client = this.Helper.GetServicesManager().CreateProxy<IRSAPIClient>(Relativity.API.ExecutionIdentity.System);
            client.APIOptions.WorkspaceID = this.Helper.GetActiveCaseID();
            var field = client.Repositories.Field.ReadSingle(Constants.GUID.DocumentSignaturesCountedBE);
            var script = Files.DocumentPageInteraction.Replace("{{FieldArtifactID}}", field.ArtifactID.ToString());
            RegisterStartupScriptBlock(new ScriptBlock() { Key = $"voteCasting_{ActiveArtifact.ArtifactID}", Script = script });
            return retVal;
        }
    }
}
