﻿var map = new Vue({
    el: '#map',
    data: {
        hoverCounty: null,
        dataAvailable: false,
        counties: {}
    },
    methods: {
        init: function () {
            var payload = {
                "artifactType": {
                    "Guids": ["2698098a-c4c8-485b-9046-0b5e465dea1f"]
                },
                "query": {
                    "fields": [{
                        "Name": "Name"
                    }, {
                        "Name": "Signatures Threshold"
                    }, {
                        "Name": "Signatures Counted B&E"
                    }, {
                        "Name": "Threshold Passed"
                    }]
                },
                "start": 1,
                "length": 100
            };
            var headers = {
                headers: {
                    'X-CSRF-Header': '.'
                }
            };
            var workspaceID = new URLSearchParams(document.location.search).get('AppID');
            this.$http.post('/Relativity.Rest/API/Relativity.Objects/workspaces/' + workspaceID + '/objects/query', payload, headers).then(function (response) {
                if (response.body.Success) {
                    this.dataAvailable = true;
                    setData(response.body.Results);
                }
            });

        },
        hoverState: function (name) {
            this.hoverCounty = this.counties.find(function (x) {
                return x.name == name;
            });
        }
    }
});
function setData(rdos) {
    map.counties = rdos.map(function (x) {
        var countyName = x.Artifact.FieldValuePairs[0].Value;
        var passed = x.Artifact.FieldValuePairs[3].Value;
        jQuery('#' + countyName).addClass(passed ? 'passed' : 'not-passed');
        return {
            name: countyName,
            passed: passed,
            threshold: x.Artifact.FieldValuePairs[1].Value,
            countedBE: x.Artifact.FieldValuePairs[2].Value
        }
    });
}
map.init();
jQuery('svg').children()
	.hover(function () {
	    jQuery(this).toggleClass('highlight');
	    map.hoverState(this.id);
	},
   function () {
       jQuery(this).toggleClass('highlight')
   })

