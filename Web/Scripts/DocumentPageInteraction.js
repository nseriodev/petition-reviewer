﻿var $local = jQuery;
/*Inject code in document viewer frame*/
var $top = window.top.jQuery;
var targetInput = null;

function increment() {
    searchInput();
    if (targetInput != null) {
        var currentVal = getNumericValue(targetInput);
        targetInput.val(currentVal + 1);
        targetInput.trigger('change');
    }
}
function decrease() {
    searchInput();
    if (targetInput != null) {
        var currentVal = getNumericValue(targetInput);
        if (currentVal > 0) {
            targetInput.val(currentVal - 1);
            targetInput.trigger('change');
        }
    }
}

function deleteIcon(jq, elem) {
    jq(elem).parent().remove();
    decrease();
}

function getNumericValue(input) {
    var currentVal = input.val();
    var numericValue = Number(currentVal);
    return isNaN(numericValue) ? 0 : numericValue;
}

function searchInput() {
    var profileFrame = $top('#_profileAndPaneCollectionFrame')[0].contentWindow.jQuery('#_documentProfileFrame');
    if (profileFrame.length > 0) {
        var $editor = profileFrame[0].contentWindow.jQuery;
        var hiddenInput = $editor('input[faartifactid="' + petitionReviewerField + '"]');
        if (hiddenInput.length == 1) {
            targetInput = hiddenInput.closest('tr').find('input[type="text"]');
        }
    }
}

/*Image viewer loading*/
$top('#_documentViewer__originalImageFrame').on('load', function () {
    var viewerWindow = this.contentWindow;
    var $d = viewerWindow.jQuery;
    var imageViewerIframe = $d('iframe.originalImageViewerIframe');
    imageViewerIframe.ready(function () {
        var $viewer = imageViewerIframe[0].contentWindow.jQuery;
        var imageViewerDoc = imageViewerIframe[0].contentWindow.document;
        $viewer('#viewer').css({ position: 'relative' });
        setTimeout(function () {
            $viewer(imageViewerDoc).on('click', '#viewer .pageContainer', function (e) {
                if (e.ctrlKey) {
                    var container = $d('<div style="position: absolute;width: 24px; height: 24px; z-index: 100; background-image: url(\'/Relativity/images/success_icon_24px.png\');"></div>');
                    var closeicon = $d('<div title="Remove" style="position: absolute;width: 16px; height: 16px; margin: -7px 0 0 17px; cursor: pointer; background-image: url(\'/Relativity/images/grey_close.png\');">&nbsp;</div>');
                    closeicon.click(function () { deleteIcon($d, closeicon) });
                    container.css({
                        left: e.offsetX,
                        top: e.offsetY + this.offsetTop,
                    });
                    $(this).parent().append(container.append(closeicon));
                    increment();
                }
            })
        }
		, 500);
    });
});

/*Native viewer*/
$top('#_documentViewer__viewerFrame').on('load', function () {
    //$top('iframe.originalImageViewerIframe').on('load', function() {
    var viewerDoc = this.contentWindow;
    //console.log('document viewer frame loaded');
    setTimeout(function () {
        var $d = viewerDoc.jQuery;
        console.log($d('#viewer').length);
        //$d('#viewer div.pageContainer').on('click', function(e){
        $d(viewerDoc.document).on('click', '#viewer', function (e) {
            if (e.ctrlKey) {
                //var container = $d('<div style="position: absolute;width: 24px; height: 24px; background-image: url(\'/Relativity/images/grey_close.png\');">&nbsp</div>');
                var container = $d('<div style="position: absolute;width: 24px; height: 24px; z-index: 100;background-image: url(\'/Relativity/images/success_icon_24px.png\');">&nbsp</div>');
                container.css({
                    left: e.offsetX - 12,
                    top: e.offsetY - 12,
                });
                $(this).append(container);
                increment();
            }
        });
    }, 500);
});

