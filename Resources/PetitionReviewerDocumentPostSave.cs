﻿using System;
using kCura.EventHandler;
using System.Data.SqlClient;
using kCura.Relativity.Client;
using System.Runtime.InteropServices;
using kCura.EventHandler.CustomAttributes;

namespace BE.PetitionReviewer.Resources
{

    [Guid("954ED4EF-D4BB-4E85-B3B3-DF383C9B4CED")]
    [Description("Petition Reviewer Document Post Save")]
    public class PetitionReviewerDocumentPostSave : PostSaveEventHandler
    {
        public override FieldCollection RequiredFields
        {
            get
            {
                return new FieldCollection()
                {
                    new kCura.EventHandler.Field(Constants.GUID.DocumentCounty),
                    new kCura.EventHandler.Field(Constants.GUID.DocumentPayee),
                    new kCura.EventHandler.Field(Constants.GUID.DocumentCirculator)
                };
            }
        }

        public override Response Execute()
        {
            string message = string.Empty;
            try
            {
                var client = this.Helper.GetServicesManager().CreateProxy<IRSAPIClient>(Relativity.API.ExecutionIdentity.System);
                client.APIOptions.WorkspaceID = this.Helper.GetActiveCaseID();
                var caseContext = this.Helper.GetDBContext(this.Helper.GetActiveCaseID());
                var countyField = client.Repositories.Field.ReadSingle(Constants.GUID.DocumentCounty);
                var county = this.ActiveArtifact.Fields[countyField.ArtifactID];
                if (!county.Value.IsNull)
                {
                    var countyValue = (int)county.Value.Value;
                    caseContext.ExecuteNonQuerySQLStatement(Files.UpdateCountySignatures, new SqlParameter[] {
                        new SqlParameter("CountyArtifactId", countyValue)
                    });
                }
                var payeeField = client.Repositories.Field.ReadSingle(Constants.GUID.DocumentPayee);
                var payee = this.ActiveArtifact.Fields[payeeField.ArtifactID];
                if (!payee.Value.IsNull)
                {
                    var payeeValue = (int)payee.Value.Value;
                    caseContext.ExecuteNonQuerySQLStatement(Files.UpdatePayeeSignatures, new SqlParameter[] {
                        new SqlParameter("PayeeArtifactId", payeeValue)
                    });
                }
                var circulatorField = client.Repositories.Field.ReadSingle(Constants.GUID.DocumentCirculator);
                var circulator = this.ActiveArtifact.Fields[circulatorField.ArtifactID];
                if (!circulator.Value.IsNull)
                {
                    var circulatorValue = (int)circulator.Value.Value;
                    caseContext.ExecuteNonQuerySQLStatement(Files.UpdateCirculatorSignatures, new SqlParameter[] {
                        new SqlParameter("CirculatorArtifactId", circulatorValue)
                    });
                }
            }
            catch (Exception ex)
            {
                message = ex.ToString();
            }
            var response = new Response();
            response.Message = message;
            response.Success = string.IsNullOrWhiteSpace(message);
            return response;
        }
    }
}
