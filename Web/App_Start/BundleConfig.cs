﻿using System.Web.Optimization;

namespace Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/map").Include(
                      "~/Scripts/jquery-2.2.4.min.js",
                      "~/Scripts/vue.js",
                      "~/Scripts/vue-resource.min.js",
                      "~/Scripts/map.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/map.css"));
            BundleTable.EnableOptimizations = true;
        }
    }
}
