﻿using System;

namespace BE.PetitionReviewer.Resources
{
    public class Constants
    {
        public static class GUID
        {
            public static Guid DocumentSignaturesCountedBE = new Guid("1D010B90-65D1-46B8-AB03-F06C0E73C719");
            public static Guid DocumentCounty = new Guid("D1FFE5AE-3C38-4ADE-B758-7355C5D622D4");
            public static Guid DocumentPayee = new Guid("753BED0B-EB34-4110-B3DE-45C010518423");
            public static Guid DocumentCirculator = new Guid("10F2EC73-C3C8-4974-A637-F1C7DB13E4E6");

        }
    }
}
